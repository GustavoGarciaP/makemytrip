package com.mmtrip.testcases;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mmtrip.pages.Page_008;
import com.mmtrip.resources.Base;



public class Jira_008 extends Base {
	
	public WebDriver driver;
	String picker = "2";
	
	@BeforeTest
	public void startDriver() throws IOException{
		driver = InizializeDriver();
		driver.manage().window().maximize();
		
	}
	
	@Test
	public void baseNavigation() throws IOException, InterruptedException {
				
		
		 driver.get("https://www.makemytrip.com/"); //
		 //driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 
		 //String newcurrent = driver.getCurrentUrl();
			//String logexpected = "https://www.kayak.com.co/?ispredir=true";
			
			//Assert.assertEquals(newcurrent, logexpected);
			//System.out.println("Correct page");
		
		 Page_008 ht = new Page_008(driver);
		 ht.getPop().click();
		
		 
		 ht.getCOpts().click();
		 ht.getCountry().click();
		 Thread.sleep(3000);
		 ht.getHotels().click();
		 
		 ht.getOrigin().click();
		 ht.getInputD().sendKeys("Phuket");
		 
		 ht.getOpt().click();

		 ht.getDate().click();
		 
		 ht.getReturn().click();
		 
		 ht.getRoomGuests().click();
		 Thread.sleep(3000);
		 
		 ht.getAdults().click();
		 ht.getChildren().click();
		 
		 
		 
		 ht.getAge(picker);
	 
		/* WebElement staticDropdown = driver.findElement(By.xpath("//*[@id=\'0\']"));
		 Select dropdown =new Select(staticDropdown);
			//dropdown.selectByIndex(3); //selects the fourth option of the index
			dropdown.selectByVisibleText("4");*/
		 
		ht.applyFeatures().click();
		ht.getReason().click();
		
		ht.getLeisure().click();
		 
		ht.searchHotels().click();
		
		
		 
		List<WebElement> links = driver.findElements(By.xpath("//a[@rel='nofollow']"));
		
		
		
		System.out.println("The search throws "+ links.size() + " properties");
		
		for (WebElement link:links) {
			
			
			
			System.out.println(link.getAttribute("href"));
			
		}
		
		
		
			
			 File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			 FileUtils.copyFile(src,new File("C:\\Users\\User\\Documents\\makemytrip\\screenshots\\jira_008.png"));
			 
		 
	}
	
	
	@AfterTest
	public void closedriver(){
	 
		driver.close();
	 
	}
	
}

