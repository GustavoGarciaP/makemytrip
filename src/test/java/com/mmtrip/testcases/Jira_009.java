package com.mmtrip.testcases;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mmtrip.pages.Page_009;
import com.mmtrip.resources.Base;



public class Jira_009 extends Base {
	
	public WebDriver driver;
	
	@BeforeTest
	public void startDriver() throws IOException{
		driver = InizializeDriver();
		driver.manage().window().maximize();
		
	}
	
	@Test
	public void baseNavigation() throws IOException, InterruptedException {
				
		
		 driver.get("https://www.makemytrip.com/"); 
		 
		
		 Page_009 ts = new Page_009(driver);
		
		 ts.getPop().click();
		
		 
		 ts.getCOpts().click();
		 
		 ts.getCountry().click();
		 
		 ts.getTrains().click();
		 
		 ts.getLiveStat().click();
		 
		 ts.getTrain().click();
		
		 ts.getOrigin().sendKeys("12724");
		 
		 ts.setOrigin().click();
		 Thread.sleep(3000);
			
		 ts.setTStop().click();
		 
		 ts.setDate().click();
		 
		 ts.checkStatus().click();
		 
		 
		 System.out.println(ts.getTrainHour());
		 
			
		 File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		 FileUtils.copyFile(src,new File("C:\\Users\\User\\Documents\\makemytrip\\screenshots\\jira_009.png"));
		
}
	
	@AfterTest
	public void closedriver(){
	 
		driver.close();
	 
	}
	
}

