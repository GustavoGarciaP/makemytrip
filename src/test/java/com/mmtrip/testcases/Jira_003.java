package com.mmtrip.testcases;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mmtrip.pages.Page_003;
import com.mmtrip.resources.Base;



public class Jira_003 extends Base {
	
	public WebDriver driver;
	
	@BeforeTest
	public void startDriver() throws IOException{
		driver = InizializeDriver();
		driver.manage().window().maximize();
		
	}
	
	@Test
	public void baseNavigation() throws IOException, InterruptedException {
				
		
		 driver.get("https://www.makemytrip.com/flights/?ccde=us"); //
		
		 
		
			
		//System.out.println(currentUrl);
		
		
		 Thread.sleep(3000);
		 
		 Page_003 rt = new Page_003(driver);
		 rt.getPop().click();
		 
		 rt.getRt().click();
		 	 
		 
		 rt.getOrigin().click();
		 
		 rt.getOpt().click();
		// Thread.sleep(3000);
		 
		 rt.getInputD().sendKeys("Bangkok");
		 rt.getDCity().click();
		 
		 Thread.sleep(3000);
		 
		 rt.getDate().click();
		 rt.getReturn().click();
		 
		 rt.getTravClass().click();

		 rt.getAdults().click();
		 
		 rt.getChildren().click();
		 
		 rt.getTClass().click();
		 
		 rt.applyFeatures().click();
		 
		 rt.searchFlights().click();
		 
		 
		
		
			
			File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src,new File("C:\\Users\\User\\Documents\\makemytrip\\screenshots\\Jira_003.png"));
			 
		 
	}
	@AfterTest
	public void closedriver(){
	 
		driver.close();
	 
	}
	
}

