package com.mmtrip.testcases;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mmtrip.pages.Landing;
import com.mmtrip.resources.Base;



public class Test001 extends Base {
	
	public WebDriver driver;
	
	@BeforeTest
	public void startDriver() throws IOException{
		driver = InizializeDriver();
		driver.manage().window().maximize();
		
	}
	
	@Test
	public void baseNavigation() throws IOException, InterruptedException {
				
		
		 driver.get("https://www.makemytrip.com/"); //
		 //driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 
		 //String newcurrent = driver.getCurrentUrl();
			//String logexpected = "https://www.kayak.com.co/?ispredir=true";
			
			//Assert.assertEquals(newcurrent, logexpected);
			//System.out.println("Correct page");
		
		 
		 Landing lp = new Landing(driver);
		 
		 lp.getPop().click();//clicks to hide popup
		 
		 lp.getCountry().click();

		 lp.getUsa().click();// click on usa
				 
		Thread.sleep(3000);
		
		 String newcurrent = driver.getTitle();// gets title of the page
		 String title = "MakeMyTrip USA - #1 Travel Website for Flight Booking, Airline Tickets";

		 		 
		
		 try {
			    Assert.assertEquals(newcurrent, title);
			} catch (AssertionError e) {
			    System.out.println("Incorrect");
			    throw e;
			}
			System.out.println("Correct");
			
			
			
			 File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			 FileUtils.copyFile(src,new File("C:\\Users\\User\\Documents\\makemytrip\\screenshots\\test001.png"));
			 
		 
	}
	/*@AfterTest
	public void closedriver(){
	 
		driver.close();
	 
	}*/
	
}

