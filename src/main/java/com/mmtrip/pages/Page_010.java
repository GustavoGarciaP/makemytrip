package com.mmtrip.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class Page_010 {
	
public WebDriver driver;
	
	public Page_010(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		}
	
	By avoidPop = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[5]/div[2]");
	
	By countryOpts = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[6]/div[1]/p[2]/span[3]");
	
	By country = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[6]/div[2]/p[1]/span[2]");
	
	By moreOpts = By.xpath("//*[@id=\'SW\']/div[1]/div[2]/div/div/nav/ul/li[10]");
	
	By giftCards = By.xpath("//a[@data-cy='submenu_Giftcards']");
	
	By motherCard = By.xpath("//*[@id=\'root\']/div/div[2]/div/div[1]/div/div[2]/div[7]/ul/li[3]");
	

	
	public WebElement getPop() {
		
		return driver.findElement(avoidPop);
	
				
	}
	
	public WebElement getCOpts() {
		
		return driver.findElement(countryOpts);
	
				
	}

	public WebElement getCountry() {
	return driver.findElement(country);
	}
	
	
	public WebElement getGift() {
		return driver.findElement(giftCards);
	}
	
	public WebElement getMCard() {
		return driver.findElement(motherCard);
	}
	
	
	
	

}
