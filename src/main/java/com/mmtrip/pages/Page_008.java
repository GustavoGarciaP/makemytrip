package com.mmtrip.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ISelect;
import org.openqa.selenium.support.ui.Select;

public class Page_008 {
	public WebDriver driver;
	
	public Page_008(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		}
		
	
		By avoidPop = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[5]/div[2]");
		
		
		
		By countryOpts = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[6]/div[1]/p[2]/span[3]");
		
		By country = By.xpath("//*[@id=\'SW\']/div[1]/div[1]/ul/li[6]/div[2]/p[1]/span[2]");
		
		By hotels = By.xpath("//*[@id=\'SW\']/div[1]/div[2]/div/div/nav/ul/li[2]");
		
		
			
		
		By inputOrigin = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/div/div/div[1]/label/span");
		
		
		By pickOrigin = By.xpath("//*[@id=\'react-autowhatever-1\']//ul/li/div/div/p[contains(text(),'New York, US')]");
		
		
		By destination = By.xpath("//span[contains(text(),'To')]");
		
		By inputDestiny = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div/input");
		
		By phuket = By.xpath("//*[@id=\'react-autowhatever-1\']/div[1]/ul//li//div//p[contains(text(), 'Phuket, Thailand')]");
		
		
		
		By currentDate = By.cssSelector("div.DayPicker-Day--today");

		By returnDate = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/div[3]/div[4]/div[6]");
		
		By roomGuests = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/div/div/div[4]/label");
		
		By adults = By.xpath("//li[@data-cy='adults-3']");
		
		By children = By.xpath("//li[@data-cy='children-1']");
		

		By buttonApply = By.cssSelector("button.btnApply");
		
		By travelReason = By.xpath("//*[@id=\'root\']/div/div[2]/div/div/div[2]/div/div/div[5]/label/span");
		
		By leisure = By.xpath("//li[@data-cy='travelFor-Leisure']");
		By searchBtn = By.cssSelector("button#hsw_search_button");
		
		By childAge = By.xpath("//*[@id=\'0\']");

		/* WebElement staticDropdown = driver.findElement(By.xpath("//*[@id=\'0\']"));
		 Select dropdown =new Select(staticDropdown);
			//dropdown.selectByIndex(3); //selects the fourth option of the index
			dropdown.selectByVisibleText("4");*/
		
		
				
		
		//*[@class="Listing_hotel_0"]/a
		public WebElement getPop() {
			
			return driver.findElement(avoidPop);
		
					
		}
		
		public WebElement getCOpts() {
			
			return driver.findElement(countryOpts);
		
					
		}
	
		public WebElement getCountry() {
		return driver.findElement(country);
		}
		
		public WebElement getHotels() {
			return driver.findElement(hotels);
			}
		
		
		
	
		public WebElement getOrigin() {
	
			return driver.findElement(inputOrigin);

			
		}
		
		public WebElement getOpt() {
			
			return driver.findElement(phuket);

			
		}
		
		
		
		
		public WebElement getInputD() {
			
			return driver.findElement(inputDestiny);

			
		}
		
		public WebElement getDate() {
			
			return driver.findElement(currentDate);

			
		}
		public WebElement getReturn() {
			
			return driver.findElement(returnDate);

			
		}
		
        public WebElement getRoomGuests() {
			
			return driver.findElement(roomGuests);

			
		}
		public WebElement getAdults() {
			
			return driver.findElement(adults);

			
		}
		public WebElement getChildren() {
			
			return driver.findElement(children);

			
		}
		

		
		
		public WebElement applyFeatures() {
			
			return driver.findElement(buttonApply);

			
		}
		

		public WebElement getReason() {
			
			return driver.findElement(travelReason);

			
		}
		

		public WebElement getLeisure() {
			
			return driver.findElement(leisure);

			
		}
		
		
		public WebElement searchHotels() {
			
			return driver.findElement(searchBtn);

			
		}
	
	//pass the id to a variable then turn it to an object.
	
	
		public void getAge(String pick) {
			
			WebElement staticDropdown = driver.findElement(childAge);
			 Select dropdown =new Select(staticDropdown);
				
				dropdown.selectByVisibleText(pick);
			
			
		
					
		}
		
	
	
	
	
	

}
