package com.mmtrip.resources;

import java.io.FileInputStream;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Base {
	
	public WebDriver driver;
	public Properties props;

	public WebDriver InizializeDriver() throws IOException {
		
		Properties props = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\User\\Documents\\makemytrip\\src\\main\\java\\com\\mmtrip\\resources\\data.properties");
		props.load(fis);
		String browserName = props.getProperty("browser");
		
		if(browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver","C:\\Users\\User\\Documents\\makemytrip\\drivers\\chromedriver.exe" );
			driver=new ChromeDriver();
		}
		else if(browserName.equals("firefox")){
			System.setProperty("webdriver.chrome.driver","C:\\Users\\User\\Documents\\makemytrip\\drivers\\geckodriver.exe" );
			driver=new FirefoxDriver();
			
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
		
	
	}
	
}
